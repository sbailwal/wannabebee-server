/**
 * Models are saved using mongoose schema
 *  */ 

import mongoose from "mongoose";

// Setup schema
const wordSchema = mongoose.Schema({
    word: String,
    pronunciation: String,
    definition: String,
    origin: String,
    partOfSpeech: String,
    example: String,
});

// Attaching schema to actual model/document in mongo
const Word = mongoose.model('words', wordSchema);    

export default Word;
