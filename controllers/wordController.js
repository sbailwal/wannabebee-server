/**
 * This is our Controller w.r.t MVC architecture
 */

import Word from '../models/word.js';

// default max row count 
const defaultRowCount = 6;

// TBD: Not sure how to write this in ES6 yet
module.exports.controller = (app) => {

    /**  
     * POST: Add a word
     * payload: 
     *     { "word": "test", "description": "","origin": "", 
     *       "partOfSpeech": "", "example": "" 
     *     }
    */
    app.post('/words', (req, res) => {
        const newWord = new Word({
            word: req.body.word,
            pronunciation: req.body.pronunciation,
            definition: req.body.definition,
            origin: req.body.origin,
            partOfSpeech: req.body.partOfSpeech,
            example: req.body.example,
        });
        newWord.save((error, word) => {
            if (error) { console.log(error); }
            res.send(word);
        }); 
    });

    /**  
     * GET: Fetch based on filters: 
     * Query String: rowCount, queryFilters
    */
    app.get('/words', (req, res) => {
        // setting resultset limit, (prepend with '+' to convert to number)        
        const rowCount = +((isNaN(req.query.rowCount)) ? defaultRowCount : req.query.rowCount );
        console.log(`Max result set to:${rowCount}`);

        // filters
        const queryFilters = req.query.queryFilters ? req.query.queryFilters : '{}'; // {origin: 'latin'};
        console.log(`queryFilters set to:${queryFilters}`);

        // Test: printing out query string. delete later
        for (const key in req.query) {
            console.log(`${key}(key),${req.query[key]}(value)`);
        }

        // Returns random words 
        Word.aggregate([
            { $match: JSON.parse(queryFilters) }, // {"origin": "latin"}
            { $sample: {size: rowCount} },
            // itemsSold: { $addToSet: "$item" } // for distinct values
            // { $limit: rowCount }, // for limiting resultset
        ], (err, words) => {
            if (err) { console.log(`Error retrieving words. Error Stack: ${err}`)};
            res.send({ words,});
        });
    });

    /**  
     * GET: Fetch all words: 
     * params: rowCount
    */
    // app.get('/words', (req, res) => {
    //     const rowCount = req.body.rowCount ? req.body.rowCount : defaultMaxRowCount;
    //     Word.find(
    //         {}, 
    //         'word pronunciation definition origin partOfSpeech example', 
    //         (err, words) => {
    //             if (err) { console.log(`Error retrieving words. Error Stack: ${err}`)};
    //             res.send({
    //                 words,
    //             });
    //         }
    //     ).limit(rowCount);
    // });

    /**  
     * GET: Fetch specific word by id: 
     * URL parameter: id
    */
    app.get('/words/:id', (req, res) => {
        Word.findById(
            req.params.id, 
            'word pronunciation definition origin partOfSpeech example pronunciation', 
            (err, word) => {
                if (err) { console.error(`Error retrieving word details. Error Stack: ${err}`)};
                res.send(word);
            }
        );
    });

    // app.get('/wordstest', (req, res) => {
    //     res.json([
    //         {
    //             word: 'nostalgic',
    //             definition: 'sweet memory',
    //             origin: 'latin',
    //             partOfSpeech: 'noun',
    //             example: 'The nostalgia of burgers is very sweet to me',
    //             pronunciation: '[ri-lent-lis]',
    //         },
    //         {
    //             word: 'arrogant',
    //             definition: 'very self absorbed',
    //             origin: 'latin',
    //             partOfSpeech: 'adjective',
    //             example: 'The arrogance of wealth is good to have.',
    //             pronunciation: '[ri-lent-lis]',
    //         }    
    //     ]);
    // });
};

// export default WordController;
