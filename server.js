import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import morgan from "morgan";    // for logging
import fs from "fs";

// Initialize the app
const app = express();
const router = express.Router();

// Initialize middleware
app.use(bodyParser.urlencoded({
    extended: true
 }));
app.use(bodyParser.json());
app.use('/', router);
app.use(morgan('combined'));
app.use(cors());

// Database: connect to mongodb, must use the useNewUrlParser for mongo >=3.1.0
mongoose.connect('mongodb://localhost:27017/wannabebee', { useNewUrlParser: true }, function(err) {
    if (err) {
        console.error(
            `DB Connection Error: Check if MongoDB is running.
            Error: ${err.stack}`
        );
        console.error('Shutting the server... Fix and restart');
        process.exit(1);
    }
    else {
        console.log('Connection to MongoDB has been made');
    }
});

// mongoose.connection.on('error', function(err) {
//     console.error('DB Error: Could not connect. Check if MongoDB deamon is running, Error:', err.stack);
// });

// Log all queries fired by Mongoose
mongoose.set('debug', true);

// Including/importing all files under controllers folder
fs.readdirSync("controllers").forEach(function (file) {
    if(file.substr(-3) == ".js") {
      const route = require("./controllers/" + file);
      route.controller(app);
} })

// API routes in the app
router.get('/', function(req, res) {
    res.json({ message: 'API Initialized!'});
});

const port = process.env.API_PORT || 3001;

// Launch app and listen to the specified port
app.listen(port, function() {
    console.log(`api running on port ${port}`);
});
